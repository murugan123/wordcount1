import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class WordMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

	@Override
	public void map(LongWritable Key, Text value,OutputCollector<Text, IntWritable> output, Reporter r) throws IOException {
		Spechar sc = new Spechar();
		String s=value.toString();
		for(String word:s.split(" ")) {
			boolean b=sc.check(word);
			if (b == false & word.length()==6) {
				output.collect(new Text (word), new IntWritable(1));
			}
		}
		
	}
	
	
	

}
